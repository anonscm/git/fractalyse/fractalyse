/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.thema.fracgis.method.vector.mono;

import java.util.List;
import java.util.TreeMap;
import org.locationtech.jts.geom.Coordinate;
import org.thema.common.ProgressBar;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.data.feature.Feature;
import org.thema.data.feature.FeatureCoverage;
import org.thema.fracgis.sampling.RadialSampling;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.SimpleParallelTask;

/**
 * Calculates GWFA dimension for vector point data.
 * 
 *
 * @author Gilles Vuidel
 */
public class GWFAMethod extends MonoVectorMethod {
    

    /**
     * Creates a new GWFA method for the given vector data
     * @param inputName the input data layer name 
     * @param sampling the scale sampling
     * @param coverage the input vector data, must be Point geometry
     */
    public GWFAMethod(String inputName, RadialSampling sampling, FeatureCoverage<Feature> coverage) {
        super(inputName, sampling, coverage);
    }

    /**
     * For parameter management only
     */
    public GWFAMethod() {
    }

    @Override
    public void execute(ProgressBar monitor, boolean parallel) {
        final DefaultFeatureCoverage<DefaultFeature> pointCov = CorrelationMethod.flattenPoints(getCoverage().getFeatures());
        final double [] tot = new double[getSampling().getValues().size()]; 
        
        SimpleParallelTask<DefaultFeature, double[]> task = new SimpleParallelTask<DefaultFeature, double[]>(pointCov.getFeatures(), monitor) {
            @Override
            protected double[] executeOne(DefaultFeature elem) {
                double w = getWeight(elem.getGeometry().getCoordinate());
                if(Double.isNaN(w)) {
                    return null;
                }
                
                int [] count = CorrelationMethod.calcOne(elem.getGeometry(), pointCov, getSampling());
                double [] res = new double[count.length];
                
                for(int i = 0; i < res.length; i++) {
                    res[i] = count[i]*w;
                }
                return res;
            }
            
            @Override
            public void gather(List<double[]> results) {
                for(double [] m : results) {
                    if(m != null) {
                        for(int i = 0; i < tot.length; i++) {
                            tot[i] += m[i];
                        }
                    }
                }
            }
        };
        
        if(parallel) {
            ExecutorService.execute(task);
        } else {
            ExecutorService.executeSequential(task);
        }
        
        curve = new TreeMap<>();
        for(int i = 1; i < tot.length; i++) {
            tot[i] += tot[i-1];
        }
        double totWeight = 0;
        for(DefaultFeature f : pointCov.getFeatures()) {
            double w = getWeight(f.getGeometry().getCoordinate());
            if(!Double.isNaN(w)) {
                totWeight += w;
            }
        }
        
        int i = 0;
        for(Double val : getSampling().getValues()) {
            curve.put(val, tot[i++] / totWeight);
        }
    }
    
    private double getWeight(Coordinate point) {
        double dist = getCentre().distance(point);
        double max = getSampling().getMaxSize()/2;
        if(dist >= max) {
            return Double.NaN;
        }
        
        return Math.pow(1 - Math.pow(dist / max, 2), 2);
    }

    @Override
    public int getDimSign() {
        return 1;
    }
    
    @Override
    public String getName() {
        return "GWFA";
    }
    
    public Coordinate getCentre() {
        return ((RadialSampling)getSampling()).getCentre();
    }
    
}
