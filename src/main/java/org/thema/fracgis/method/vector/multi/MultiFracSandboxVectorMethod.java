/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fracgis.method.vector.multi;

import org.thema.fracgis.method.MultiFracMethod;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.thema.common.ProgressBar;
import org.thema.data.feature.DefaultFeature;
import org.thema.data.feature.DefaultFeatureCoverage;
import org.thema.data.feature.FeatureCoverage;
import org.thema.fracgis.method.MultiFracSandboxCurves;
import org.thema.fracgis.method.vector.VectorMethod;
import org.thema.fracgis.method.vector.mono.CorrelationMethod;
import org.thema.fracgis.sampling.DefaultSampling;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.SimpleParallelTask;

/**
 * Multifractal analysis with correlation and vector data.
 * 
 * @author Gilles Vuidel
 */
public class MultiFracSandboxVectorMethod extends VectorMethod implements MultiFracMethod {
    
    private MultiFracSandboxCurves curves;
    private List<Coordinate> centres;
    
    /**
     * Creates a new multifractal sandbox method for vector data.
     * @param inputName the input data layer name 
     * @param sampling the scale sampling
     * @param cover the input vector data
     */
    public MultiFracSandboxVectorMethod(String inputName, DefaultSampling sampling, FeatureCoverage cover) {
        super(inputName, sampling, cover);
        this.centres = null;
    }

    /**
     * Set starting counting centres
     * @param centres 
     */
    public void setCentres(List<Coordinate> centres) {
        this.centres = centres;
    }

    @Override
    public void execute(ProgressBar monitor, boolean parallel) {
        DefaultFeatureCoverage<DefaultFeature> pointCov = CorrelationMethod.flattenPoints(getCoverage().getFeatures());
        List<DefaultFeature> centreFeatures;
        if(centres != null) {
            GeometryFactory factory = new GeometryFactory();
            centreFeatures = new ArrayList<>();
            for(Coordinate c : centres) {
                centreFeatures.add(new DefaultFeature(c, factory.createPoint(c)));
            }
        } else {
            centreFeatures = pointCov.getFeatures();
        }
        
        List<double[]> count = new ArrayList<>();
        
        SimpleParallelTask<DefaultFeature, double[]> task = new SimpleParallelTask<DefaultFeature, double[]>(centreFeatures, monitor) {
            @Override
            protected double[] executeOne(DefaultFeature elem) {
                int [] nb = CorrelationMethod.calcOne(elem.getGeometry(), pointCov, getSampling());
                double [] m = new double[nb.length];
                m[0] = nb[0];
                for(int i = 1; i < nb.length; i++) {
                    m[i] = nb[i] + m[i-1];
                }
                return m;
            }
            
            @Override
            public void gather(List<double[]> results) {
                count.addAll(results);
            }
        };

        if(parallel) {
            ExecutorService.execute(task);
        } else {
            ExecutorService.executeSequential(task);
        }
        
        curves = new MultiFracSandboxCurves(count, getSampling());
    }

    @Override
    public int getDimSign() {
        return -1;
    }

    @Override
    public String getName() {
        return "MultiFractal sandbox";
    }
    
    @Override
    public TreeMap<Double, Double> getCurve(double q) {
        return curves.getCurve(q);
    }
    
    @Override
    public TreeMap<Double, TreeMap<Double, Double>> getCurves(TreeSet<Double> qs) {
        return curves.getCurves(qs);
    }
    
}
