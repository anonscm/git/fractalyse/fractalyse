/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fracgis.method.raster.multi;

import org.thema.fracgis.method.MultiFracMethod;
import org.locationtech.jts.geom.Envelope;
import java.awt.image.RenderedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;
import org.thema.common.ProgressBar;
import org.thema.fracgis.method.MultiFracSandboxCurves;
import org.thema.fracgis.method.raster.RasterMethod;
import org.thema.fracgis.method.raster.mono.RadialRasterMethod;
import org.thema.fracgis.sampling.DefaultSampling;
import org.thema.parallel.AbstractParallelTask;
import org.thema.parallel.ExecutorService;

/**
 * Multifractal analysis with boxcounting and raster data.
 *
 * @author Gilles Vuidel
 */
public class MultiFracSandboxRasterMethod extends RasterMethod implements MultiFracMethod {
    
    private MultiFracSandboxCurves curves;

    /**
     * Creates a new multifractal sandbox method for the given data
     * @param inputName input layer name (must be a raster layer)
     * @param sampling scale sampling
     * @param img raster input data
     * @param env envelope of the raster in world coordinate
     */
    public MultiFracSandboxRasterMethod(String inputName, DefaultSampling sampling, RenderedImage img, Envelope env) {
        super(inputName, sampling, img, env);
    }

    @Override
    public void execute(ProgressBar monitor, boolean parallel) {
        SortedSet<Integer> scales = getSampling().getDiscreteValues();
        int dMax = scales.last();

        AbstractParallelTask<List<double []>, List<double []>> task = new AbstractParallelTask<List<double []>, List<double []>>(monitor) {
            List<double []> count = new ArrayList<>();
            
            @Override
            public int getSplitRange() {
                return getImg().getHeight();
            }
            
            @Override
            public List<double []> execute(int start, int end) {
                List<double []> res = new ArrayList<>();
                RandomIter r = RandomIterFactory.create(getImg(), null);
                for(int y = start; y < end; y++) {
                    for(int x = 0; x < getImg().getWidth(); x++) {
                        final double val = r.getSampleDouble(x, y, 0);
                        if(val < 0) {
                            throw new RuntimeException("Negative value not permitted");
                        }
                        if(Double.isNaN(val) || val == 0) {
                            continue;
                        }     

                        double [] fy = RadialRasterMethod.calcOne(getImg(), x, y, dMax);
                        double [] curve = new double[scales.size()];
                        int i = 0;
                        for(int ind : scales) {
                            curve[i] = fy[ind];
                            i++;
                        }
                        res.add(curve);
                    }
                    incProgress(1);
                }
                r.done();
                return res;
            }
            
            @Override
            public void gather(List<double []> results) {
                count.addAll(results);
            }
            
            @Override
            public List<double []> getResult() {
                return count;
            }
        };
        
        if(parallel) {
            ExecutorService.execute(task);
        } else {
            ExecutorService.executeSequential(task);
        }
        curves = new MultiFracSandboxCurves(task.getResult(), getSampling());
    }
    
    @Override
    public TreeMap<Double, Double> getCurve(double q) {
        return curves.getCurve(q);
    }
    
    @Override
    public TreeMap<Double, TreeMap<Double, Double>> getCurves(TreeSet<Double> qs) {
        return curves.getCurves(qs);
    }

    @Override
    public int getDimSign() {
        return -1;
    }
    
    @Override
    public String getName() {
        return "MultiFractal sandbox";
    }

}
