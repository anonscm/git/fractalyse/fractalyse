/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fracgis.method.raster.mono;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import java.awt.geom.Point2D;
import java.awt.image.RenderedImage;
import java.util.Locale;
import java.util.SortedSet;
import java.util.TreeMap;
import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;
import org.thema.common.ProgressBar;
import org.thema.fracgis.estimation.RectangularRangeShape;
import org.thema.fracgis.method.MethodLayers;
import org.thema.fracgis.sampling.RadialSampling;

/**
 * Radial analysis on raster data.
 * 
 * @author Gilles Vuidel
 */
public class RadialRasterMethod extends MonoRasterMethod {
       
    /**
     * Constructor for data with spatial unit (world envelope)
     * @param inputName the input layer name (must be a binary raster layer)
     * @param sampling scale sampling
     * @param img the input raster data
     * @param envelope envelope of the raster data in world coordinate or null
     */
    public RadialRasterMethod(String inputName, RadialSampling sampling, RenderedImage img, Envelope envelope) {
        super(inputName, sampling, img, envelope);
    }
    
    @Override
    public void execute(ProgressBar monitor, boolean threaded) {
        Coordinate c = getTransform().transform(getCentre(), new Coordinate());
        SortedSet<Integer> scales = getSampling().getDiscreteValues();
        int x = (int) c.x;
        int y = (int) c.y;
        int n = scales.last();
        
        double[] count = calcOne(getImg(), x, y, n);
        
        double res = getResolution();
        curve = new TreeMap<>();
        for(int ind : scales) {
            curve.put(getSampling().getValue(ind), count[ind] * res*res);
        }
    }
    
    public static double[] calcOne(RenderedImage img, int x, int y, int max) {
        double[] count = new double[max+1];
        
        final int i1 = y-max < 0 ? 0 : y-max;
        final int i2 = y+max >= img.getHeight() ? img.getHeight()-1 : y+max;
        final int j1 = x-max < 0 ? 0 : x-max;
        final int j2 = x+max >= img.getWidth() ? img.getWidth()-1 : x+max;

        RandomIter r = RandomIterFactory.create(img, null);
        for(int i = i1; i <= i2; i++) {
            for(int j = j1; j <= j2; j++) {
                double val = r.getSampleDouble(j, i, 0);
                if(val > 0) {
                    count[Math.max(Math.abs(i-y), Math.abs(j-x))] += val;
                }
            }
        }
        r.done();
        for(int i = 1; i < count.length; i++) {
            count[i] += count[i-1];
        }
        
        return count;
    }
    
    @Override
    public int getDimSign() {
        return 1;
    }
    
    @Override
    public String getName() {
        return "Radial";
    }

    @Override
    public String getParamString() {
        return String.format(Locale.US, "cx%g_cy%g_", getCentre()!=null?getCentre().x:0.0, getCentre()!=null?getCentre().y:0.0) + super.getParamString();
    }

    @Override
    public MethodLayers getGroupLayer() {
        MethodLayers groupLayer = super.getGroupLayer(); 
        groupLayer.setScaleRangeShape(new RectangularRangeShape(new Point2D.Double(getCentre().x, getCentre().y), 0, getSampling().getRealMaxSize()));
        return groupLayer;
    }
    
    private Coordinate getCentre() {
        return ((RadialSampling)getSampling()).getCentre();
    }
}
