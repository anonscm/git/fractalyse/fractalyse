/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fracgis.method;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.thema.fracgis.sampling.DefaultSampling;

/**
 *
 * @author gvuidel
 */
public class MultiFracSandboxCurves  {
    private transient List<double []> count;
    private DefaultSampling sampling;
    private TreeMap<Double, TreeMap<Double, Double>> cacheCurves;

    public MultiFracSandboxCurves(List<double[]> count, DefaultSampling sampling) {
        this.count = count;
        this.sampling = sampling;
        cacheCurves = new TreeMap<>();
    }
    
    public synchronized TreeMap<Double, Double> getCurve(double q) {
        if(!cacheCurves.containsKey(q)) {
            calcCurves(Collections.singleton(q));
        }
        return cacheCurves.get(q);
    }
    
    public TreeMap<Double, TreeMap<Double, Double>> getCurves(TreeSet<Double> qs) {
        calcCurves(qs);
        TreeMap<Double, TreeMap<Double, Double>> curves = new TreeMap<>();
        for(Double q : qs) {
            curves.put(q, cacheCurves.get(q));
        }
        return curves;
    }
    
    private void calcCurves(Set<Double> qSet) {
        List<Double> qList = new ArrayList<>();
        for(Double q : qSet) {
            if(!cacheCurves.containsKey(q)) {
                qList.add(q);
                cacheCurves.put(q, new TreeMap<>());
            }
        }
        
        if(qList.isEmpty()) {
            return;
        }
        
        int i = 0;
        for(double size : sampling.getValues()) {
            double [] sum = new double[qList.size()];
            for(double [] nb : count) {
                double val = nb[i];
                
                for(int k = 0; k < sum.length; k++) {
                    double q = qList.get(k);
                    if(q == 1) { // information dimension D1 from Theiler 1990
                        sum[k] += Math.log(val);
                    } else {
                        sum[k] += Math.pow(val, q-1);
                    }
                }
            }  
            
            for(int k = 0; k < sum.length; k++) {
                double q = qList.get(k);
                if(q == 1) { // information dimension D1 from Theiler 1990
                    cacheCurves.get(q).put(size, Math.exp(sum[k] / count.size()));
                } else {
                    cacheCurves.get(q).put(size, sum[k] / count.size());
                }
            }
            i++;
        }
    }
    
}
