/*
 * Copyright (C) 2018 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fracgis.method.network.mono;

import java.util.TreeMap;
import org.thema.common.param.ReflectObject;
import org.thema.fracgis.method.MonoMethod;
import org.thema.fracgis.method.network.NetworkMethod;
import org.thema.fracgis.sampling.DefaultSampling;
import org.thema.graph.SpatialGraph;

/**
 *
 * @author gvuidel
 */
public abstract class MonoNetworkMethod extends NetworkMethod implements MonoMethod {
   
    
    @ReflectObject.NoParam
    protected TreeMap<Double, Double> curve = new TreeMap<>();

    
    /**
     * Creates a new MonoNetworkMethod.
     * @param inputName network layer name
     * @param sampling the scale sampling
     * @param network network spatial graph
     * @param distField distance attribute or NO_WEIGHT or LENGTH_WEIGHT
     */
    public MonoNetworkMethod(String inputName, DefaultSampling sampling, SpatialGraph network, String distField) {
        super(inputName, sampling, network, distField);
    }

    /**
     * Creates a new MonoNetworkMethod.
     * @param inputName network layer name
     * @param sampling the scale sampling
     * @param network network spatial graph
     * @param distField distance attribute or NO_WEIGHT or LENGTH_WEIGHT
     * @param massField attribute name of the mass or DIST_MASS
     * @param edgeField true if massField is an attibute of edges, false if massField is an attribute of nodes
     */
    public MonoNetworkMethod(String inputName, DefaultSampling sampling, SpatialGraph network, String distField, String massField, boolean edgeField) {
        super(inputName, sampling, network, distField, massField, edgeField);
    }
    
    /** 
     * Default constructor for batch mode
     */
    public MonoNetworkMethod() {    
    }
    
    @Override
    public TreeMap<Double, Double> getCurve() {
        return curve;
    }
    
    @Override
    public int getDimSign() {
        return 1;
    }
    
}
