/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 * http://thema.univ-fcomte.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package org.thema.fracgis.method.network.multi;

import org.thema.fracgis.method.QMonoMethod;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import org.geotools.graph.structure.Node;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.thema.common.ProgressBar;
import org.thema.fracgis.method.MonoMethod;
import org.thema.fracgis.method.MultiFracMethod;
import org.thema.fracgis.method.network.NetworkMethod;
import org.thema.fracgis.sampling.DefaultSampling;
import org.thema.graph.SpatialGraph;
import org.thema.graph.Util;
import org.thema.parallel.ExecutorService;
import org.thema.parallel.SimpleParallelTask;
import org.thema.fracgis.method.MultiFracSandboxCurves;

/**
 * Multifractal analysis with correlation and vector data.
 * 
 * @author Gilles Vuidel
 */
public class MultiFracSandboxNetMethod extends NetworkMethod implements MultiFracMethod {
    
    private MultiFracSandboxCurves curves;
    private List<Coordinate> centres = null;
    
    /**
     * Creates a new multifractal sandbox method for network data.
     * @param inputName network layer name
     * @param sampling the scale sampling
     * @param network network spatial graph
     * @param distField distance attribute or NO_WEIGHT or LENGTH_WEIGHT
     * @param massField attribute name of the mass or DIST_MASS
     * @param edgeField true if massField is an attibute of edges, false if massField is an attribute of nodes
     */
    public MultiFracSandboxNetMethod(String inputName, DefaultSampling sampling, SpatialGraph network, String distField, String massField, boolean edgeField) {
        super(inputName, sampling, network, distField, massField, edgeField);
    }
    
    /**
     * Set starting counting centres
     * @param centres 
     */
    public void setCentres(List<Coordinate> centres) {
        this.centres = centres;
    }

    @Override
    public void execute(ProgressBar monitor, boolean parallel) {
        List<double []> count = new ArrayList<>();
        
        network.setSnapToEdge(false); 
        
        List<Node> startNodes;
        if(centres != null) {
            GeometryFactory factory = new GeometryFactory();
            startNodes = new ArrayList<>();
            for(Coordinate c : centres) {
                startNodes.add(network.getNearestNodes(factory.createPoint(c)));
            }
        } else {
            startNodes = new ArrayList<>(network.getGraph().getNodes());
        }
        
        SimpleParallelTask<Node, double[]> task = new SimpleParallelTask<Node, double[]>(startNodes, monitor) {
            @Override
            protected double[] executeOne(Node n) {
                return calcFromOnePoint((Point) Util.getGeometry(n));
            }

            @Override
            public void gather(List<double[]> results) {
                count.addAll(results);
            }
        };

        if(parallel) {
            ExecutorService.execute(task);
        } else {
            ExecutorService.executeSequential(task);
        }
        
        curves = new MultiFracSandboxCurves(count, getSampling());
        
    }

    @Override
    public int getDimSign() {
        return -1;
    }

    @Override
    public String getName() {
        return "MultiFractal sandbox";
    }
    
    @Override
    public TreeMap<Double, Double> getCurve(double q) {
        return curves.getCurve(q);
    }
    
    @Override
    public TreeMap<Double, TreeMap<Double, Double>> getCurves(TreeSet<Double> qs) {
        return curves.getCurves(qs);
    }
    
}
