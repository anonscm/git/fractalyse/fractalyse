/*
 * Copyright (C) 2021 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fracgis.estimation;

import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.thema.fracgis.method.MultiFracMethod;

/**
 *
 * @author gvuidel
 */
public class MultiFracEstimation {
    
    private MultiFracMethod method;
    
    private TreeMap<Double, LogEstimation> estims;
    private TreeMap<Double, Double> Tq;
    private TreeMap<Double, Double> Dq;
    private TreeMap<Double, Double> alpha;
    private TreeMap<Double, Double> f;

    public MultiFracEstimation(MultiFracMethod method) {
        this.method = method;
    }

    public MultiFracMethod getMethod() {
        return method;
    }
    
    public void updateEstim(TreeSet<Double> qSet, double xMin, double xMax) {
        estims = new TreeMap<>();
        Tq = new TreeMap<>();
        Dq = new TreeMap<>();
        alpha = new TreeMap<>();
        f = new TreeMap<>();
        for(Double q : qSet) {
            LogEstimation estim = new LogEstimation(method.getSimpleMethod(q));
            estim.setRange(xMin, xMax);
            estims.put(q, estim);
            Tq.put(q, estim.getDimension());
            if(Math.abs(q-1) > 0.00001) {
                Dq.put(q, Tq.get(q) / (1-q));
            } else if(q == 1) {
                Dq.put(q, -Tq.get(q));
                Tq.put(q, 0.0);
            }
            if(qSet.first() != q) {
                double q1 = qSet.lower(q);
                alpha.put(q, -(Tq.get(q)-Tq.get(q1))/(q-q1));
                f.put(q, Tq.get(q) + q*alpha.get(q));
            }
        }
    }

    public Set<Double> getqSet() {
        return estims.keySet();
    }
    
    public LogEstimation getEstim(double q) {
        return estims.get(q);
    }

    public TreeMap<Double, Double> getTq() {
        return Tq;
    }

    public TreeMap<Double, Double> getDq() {
        return Dq;
    }

    public TreeMap<Double, Double> getAlpha() {
        return alpha;
    }

    public TreeMap<Double, Double> getF() {
        return f;
    }
    
    
}
