/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fracgis.method.network.multi;

import org.thema.fracgis.method.vector.multi.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.locationtech.jts.geom.Coordinate;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.swing.TaskMonitor;
import org.thema.fracgis.Data;
import static org.thema.fracgis.method.network.NetworkMethod.DIST_MASS;
import static org.thema.fracgis.method.network.NetworkMethod.LENGTH_WEIGHT;
import org.thema.fracgis.sampling.DefaultSampling;
import org.thema.parallel.ParallelExecutor;

/**
 *
 * @author Gilles Vuidel
 */
public class MultiFracSandboxNetMethodTest {
    
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        ParallelExecutor.setNbProc(4);
        ParallelFExecutor.setNbProc(4);
       
        Data.loadNetVector();
    }

    /**
     * Test of execute method, of class MultiFracSandboxVectorMethod.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");

        MultiFracSandboxNetMethod method = new MultiFracSandboxNetMethod("test", new DefaultSampling(16, 512, 2), Data.netCross64, LENGTH_WEIGHT, DIST_MASS, true);
        method.execute(new TaskMonitor.EmptyMonitor(), true);
        assertEquals(Arrays.asList((8*4+4*8)/5.0, (16*4+4*16)/5.0, (32*4+4*32)/5.0, (64*4+4*64)/5.0, 256.0, 256.0), new ArrayList<>(method.getCurve(2).values()));
        assertArrayEquals(new double [] {Math.exp((Math.log(8*4)+4*Math.log(8))/5.0), Math.exp((Math.log(16*4)+4*Math.log(16))/5.0), 
                    Math.exp((Math.log(32*4)+4*Math.log(32))/5.0), Math.exp((Math.log(64*4)+4*Math.log(64))/5.0), 256.0, 256.0}, 
                ArrayUtils.toPrimitive(method.getCurve(1).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {(1.0/(8*4)+4*1.0/8)/5.0, (1.0/(16*4)+4*1.0/16)/5.0, 
                    (1.0/(32*4)+4*1.0/32)/5.0, (1.0/(64*4)+4*1.0/64)/5.0, 1/256.0, 1/256.0}, 
                ArrayUtils.toPrimitive(method.getCurve(0).values().toArray(new Double[0])), 1e-10);

        method = new MultiFracSandboxNetMethod("test", new DefaultSampling(2, 162, 3), Data.netFrac, LENGTH_WEIGHT, DIST_MASS, true);
        method.setCentres(Arrays.asList(new Coordinate(81, 81)));
        method.execute(new TaskMonitor.EmptyMonitor(), true);
        assertArrayEquals(new double [] {4.0, 20.0, 100.0, 500.0, 2500.0}, 
                ArrayUtils.toPrimitive(method.getCurve(2).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {4.0, 20.0, 100.0, 500.0, 2500.0}, 
                ArrayUtils.toPrimitive(method.getCurve(1).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {1/4.0, 1/20.0, 1/100.0, 1/500.0, 1/2500.0}, 
                ArrayUtils.toPrimitive(method.getCurve(0).values().toArray(new Double[0])), 1e-10);
        
        method = new MultiFracSandboxNetMethod("test", new DefaultSampling(2, 54, 3), Data.netFrac, LENGTH_WEIGHT, DIST_MASS, true);
        method.setCentres(Arrays.asList(new Coordinate(81, 81), new Coordinate(27, 81), new Coordinate(81, 27), 
                new Coordinate(135, 81), new Coordinate(81, 135)));
        method.execute(new TaskMonitor.EmptyMonitor(), true);
        assertArrayEquals(new double [] {4.0, 20.0, 100.0, 500.0}, 
                ArrayUtils.toPrimitive(method.getCurve(2).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {4.0, 20.0, 100.0, 500.0}, 
                ArrayUtils.toPrimitive(method.getCurve(1).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {1/4.0, 1/20.0, 1/100.0, 1/500.0}, 
                ArrayUtils.toPrimitive(method.getCurve(0).values().toArray(new Double[0])), 1e-10);
    }


}
