/*
 * Copyright (C) 2016 Laboratoire ThéMA - UMR 6049 - CNRS / Université de Franche-Comté
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.thema.fracgis.method.vector.multi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.locationtech.jts.geom.Coordinate;
import org.thema.common.parallel.ParallelFExecutor;
import org.thema.common.swing.TaskMonitor;
import org.thema.fracgis.Data;
import org.thema.fracgis.sampling.DefaultSampling;
import org.thema.parallel.ParallelExecutor;

/**
 *
 * @author Gilles Vuidel
 */
public class MultiFracSandboxVectorMethodTest {
    
    
    @BeforeClass
    public static void setUpClass() throws IOException {
        ParallelExecutor.setNbProc(4);
        ParallelFExecutor.setNbProc(4);
       
        Data.loadVector(0.01);
    }

    /**
     * Test of execute method, of class MultiFracSandboxVectorMethod.
     */
    @Test
    public void testExecute() {
        System.out.println("execute");
        DefaultSampling sampling = new DefaultSampling(1, 16, 2);
        MultiFracSandboxVectorMethod instance = new MultiFracSandboxVectorMethod("testPoint", sampling, Data.covPoint);
        instance.execute(new TaskMonitor.EmptyMonitor(), false);
        assertEquals(Arrays.asList(1.0, 1.0, 1.0, 1.0, 1.0), new ArrayList<>(instance.getCurve(-1).values()));
        assertEquals(Arrays.asList(1.0, 1.0, 1.0, 1.0, 1.0), new ArrayList<>(instance.getCurve(0).values()));
        assertEquals(Arrays.asList(1.0, 1.0, 1.0, 1.0, 1.0), new ArrayList<>(instance.getCurve(1).values()));
        assertEquals(Arrays.asList(1.0, 1.0, 1.0, 1.0, 1.0), new ArrayList<>(instance.getCurve(2).values()));
        

        sampling = new DefaultSampling(Math.sqrt(2), Math.sqrt(2)*243, 3);
        instance = new MultiFracSandboxVectorMethod("testFrac", sampling, Data.covFrac);
        instance.setCentres(Arrays.asList(new Coordinate(121.5, 121.5)));
        instance.execute(new TaskMonitor.EmptyMonitor(), true);
        assertEquals(Arrays.asList(1.0, 5.0, 25.0, 125.0, 625.0, 3125.0), new ArrayList<>(instance.getCurve(2).values()));
        assertArrayEquals(new double [] {1.0, 5.0, 25.0, 125.0,  625.0, 3125.0}, 
                ArrayUtils.toPrimitive(instance.getCurve(1).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {1/1.0, 1/5.0, 1/25.0, 1/125.0,  1/625.0, 1/3125.0}, 
                ArrayUtils.toPrimitive(instance.getCurve(0).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {1.0/1, 1.0/25, 1./625, 1.0/15625,  1.0/(625*625), 1.0/(3125*3125)}, 
                ArrayUtils.toPrimitive(instance.getCurve(-1).values().toArray(new Double[0])), 1e-10);
        
        sampling = new DefaultSampling(Math.sqrt(2), Math.sqrt(2)*81.1, 3);
        instance = new MultiFracSandboxVectorMethod("testFrac", sampling, Data.covFrac);
        instance.setCentres(Arrays.asList(new Coordinate(121.5, 121.5), 
                new Coordinate(40.5, 40.5), new Coordinate(40.5, 202.5), new Coordinate(202.5, 40.5), new Coordinate(202.5, 202.5)));
        instance.execute(new TaskMonitor.EmptyMonitor(), true);
        assertEquals(Arrays.asList(1.0, 5.0, 25.0, 125.0, 625.0), new ArrayList<>(instance.getCurve(2).values()));
        assertArrayEquals(new double [] {1.0, 5.0, 25.0, 125.0,  625.0}, 
                ArrayUtils.toPrimitive(instance.getCurve(1).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {1/1.0, 1/5.0, 1/25.0, 1/125.0,  1/625.0}, 
                ArrayUtils.toPrimitive(instance.getCurve(0).values().toArray(new Double[0])), 1e-10);
        assertArrayEquals(new double [] {1.0/1, 1.0/25, 1./625, 1.0/15625,  1.0/(625*625)}, 
                ArrayUtils.toPrimitive(instance.getCurve(-1).values().toArray(new Double[0])), 1e-10);

        
    }


}
